from django.urls import path
from . import views

urlpatterns = [
    path('',views.homeview),
    path('registerview/',views.registerview),
    path('register/',views.register),
    path('login/',views.login),
    path('studentview/',views.studentview),
    path('studentinfo/',views.studentinfo),
    path('logout/',views.logout),
]
