from django.db import models

class student(models.Model):
    s_tag=models.CharField(max_length= 15, primary_key=True)
    pin=models.CharField(max_length= 10)
    s_name=models.CharField(max_length= 30)
    s_roll=models.CharField(max_length= 30)
    email=models.CharField(max_length= 30)
    dept=models.CharField(max_length= 5)
    session=models.CharField(max_length= 10)
    semester=models.DateField()
    balance=models.IntegerField()
class account(models.Model):
    s_tag=models.CharField(max_length= 15)
    date=models.DateField()
    amount=models.IntegerField()
    status=models.CharField(max_length= 10)
class canteen(models.Model):
    s_tag=models.CharField(max_length= 15)
    date=models.DateField()
    bill=models.IntegerField()
class library(models.Model):
    s_tag=models.CharField(max_length= 15)
    book_id=models.CharField(max_length=15)
    issue_date=models.DateField()
    return_date=models.DateField()
    fine=models.IntegerField()
class all_fee(models.Model):
    s_tag=models.CharField(max_length= 15)
    date=models.DateField()
    sfee_count=models.IntegerField()
    tfee_count=models.IntegerField()
    fee=models.IntegerField()
