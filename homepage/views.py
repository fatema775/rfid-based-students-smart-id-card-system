from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User,auth
from homepage.models import student,all_fee
from django.contrib.auth.hashers import check_password
from django.contrib import messages
from datetime import date
import math

def homeview(request):
    return render(request,'home.html')

def registerview(request):
    return render(request,'home2.html')

def register(request):
    if request.method == 'POST':
        uname=request.POST['uname']
        pwd1=request.POST['pwd1']
        pwd2=request.POST['pwd2']
        email=request.POST['email']
        if pwd1==pwd2:
            c_user=User.objects.get(username=uname, email=email)
            c_user.set_password(pwd1)
            c_user.save()
            return HttpResponseRedirect('/')
        else:
            messages.info(request,'Password does not match')
            return HttpResponseRedirect('/')

def login(request):
    if request.method == 'POST':
        uname=request.POST['uname']
        pwd=request.POST['pwd']
        user=auth.authenticate(username=uname,password=pwd)
        if user is not None:
            auth.login(request,user)
            messages.info(request,'Logged in Successfully')
            if user.last_name=="accountant":
                return HttpResponseRedirect('/accountview/')
            elif user.last_name=="ctWorker":
                return HttpResponseRedirect('/canteenview/')
            elif user.last_name=="librarian":
                return HttpResponseRedirect('/libraryview/')
            else:
                return HttpResponseRedirect('/adminview/')
        elif User.objects.filter(username=uname).exists():
            messages.info(request,'Wrong Password')
            return HttpResponseRedirect('/')
        elif check_password(pwd,request.user.password):
            messages.info(request,'Wrong Username')
            return HttpResponseRedirect('/')
        else:
            messages.info(request,'Not a valid User')
            return HttpResponseRedirect('/')
    else:
        messages.info(request,'Invalid')
        return HttpResponseRedirect('/')

def studentview(request):
    return render(request,'home3.html')

def studentinfo(request):
    pn=request.POST['pin']
    if student.objects.filter(pin=pn).exists():
        user=student.objects.get(pin=pn)
        tag=user.s_tag
        today = date.today()
        data=student.objects.raw('select * from homepage_student,homepage_all_fee where homepage_student.s_tag=homepage_all_fee.s_tag and homepage_student.pin = %s',[pn])
        info = [{'s_name':p.s_name,'dept':p.dept,'session':p.session,'sfp':p.sfee_count,'sfnp':math.ceil((((today.year-p.semester.year)*12+(today.month-p.semester.month))/6+0.5))-p.sfee_count,'tfp':p.tfee_count,'tfnp':((today.year-p.semester.year)*12+(today.month-p.semester.month))-(p.tfee_count)} for p in data]
        return render(request,'account2.html',{'item':info})
    else:
        messages.info(request,'Wrong Pin')
    return render(request,'home3.html')

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')
