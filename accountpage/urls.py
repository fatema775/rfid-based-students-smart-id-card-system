from django.urls import path
from . import views

urlpatterns = [
    path('accountview/',views.accountview),
    path('addbalance/',views.addbalance),
    path('payment/',views.payment),
    path('payfee/',views.payfee),
    #path('reportgenerate/',views.reportgenerate),
    path('viewreport/',views.viewreport),
    path('payfine/',views.payfine),
]
