from django.shortcuts import render
from django.http import HttpResponseRedirect
from homepage.models import account,student,canteen,all_fee,library
from datetime import date
import datetime
from django.contrib import messages
import serial
import math
from django.core.mail import send_mail

def accountview(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    else:
        return render(request,'account.html')

def addbalance(request):
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    user=student.objects.get(s_tag=tag_no)
    a_balance=request.POST['balance']
    n_balance=int(user.balance)+int(a_balance)
    user.balance=n_balance
    user.save()
    x=datetime.datetime.now()
    # str(x.strftime("%X"))
    msg=str(a_balance)+ 'TK Recharged Successfully at 00:05:16'+' '+str(date.today())
    messages.info(request,msg)
    mail=str(a_balance) +' TK is added to your account. Current balance is '+str(n_balance)+' TK.'
    mail_to=user.email
    send_mail('account', mail,'accnt12999@gmail.com',[mail_to])
    npb=account.objects.filter(s_tag=tag_no, status='not paid')
    for i in npb:
        if user.balance >= i.amount:
            user.balance=int(user.balance)-int(i.amount)
            user.save()
            i.status='paid'
            i.save()
    return HttpResponseRedirect('/accountview/')

def payfee(request):
    d1 = date.today()
    mfee=request.POST['mnthfee']
    m=request.POST['mnth']
    sfee=request.POST['semfee']
    s=request.POST['sem']
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    m_fee=int(mfee)*int(m)
    s_fee=int(sfee)*int(s)
    t_fee=m_fee+s_fee
    st=student.objects.get(s_tag=tag_no)
    if st is not None:
        if st.balance >= t_fee:
            if all_fee.objects.filter(s_tag=tag_no).exists():
                stfee=all_fee.objects.get(s_tag=tag_no)
                stfee.sfee_count=stfee.sfee_count+int(s)
                stfee.tfee_count=stfee.tfee_count+int(m)
                stfee.fee=stfee.fee+int(t_fee)
                stfee.save()
            else:
                new_fee=all_fee(s_tag=tag_no,date=d1,sfee_count=s,tfee_count=m,fee=t_fee)
                new_fee.save()
            st.balance=int(st.balance)-int(t_fee)
            st.save()
            mail=str(t_fee) +' TK is deducted from your account. Current balance is '+str(st.balance)+' TK.'
            mail_to=st.email
            send_mail('account', mail,'accnt12999@gmail.com',[mail_to])
        else:
            messages_info(request,'Not Enough Balance')
    else:
        messages_info(request,'Not Registered')
    return HttpResponseRedirect('/accountview/')

def viewreport(request):
    today = date.today()
    data=student.objects.raw('select * from homepage_student,homepage_all_fee where homepage_student.s_tag=homepage_all_fee.s_tag')
    info = [{'s_name':p.s_name,'dept':p.dept,'session':p.session,'sfp':p.sfee_count,'sfnp':math.ceil((((today.year-p.semester.year)*12+(today.month-p.semester.month))/6+0.5))-p.sfee_count,'tfp':p.tfee_count,'tfnp':((today.year-p.semester.year)*12+(today.month-p.semester.month))-(p.tfee_count)} for p in data]
    return render(request,'account2.html',{'item':info})

def payment(request):
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    user=canteen.objects.filter(s_tag=tag_no,date=d1).order_by('-id')[0]
    print(user)
    amount=user.bill
    st=student.objects.get(s_tag=tag_no)
    if st.balance >= amount:
        st.balance=int(st.balance)-int(amount)
        st.save()
        acc=account(s_tag=tag_no,date=d1,amount=amount,status="paid")
        acc.save()
        # mail=str(amount) +' TK is deducted from your account. Current balance is '+str(st.balance)+' TK.'
        # mail_to=st.email
        # send_mail('account', mail,'accnt12999@gmail.com',[mail_to])
        return HttpResponseRedirect('/canteenview/')
    else:
        acc=account(s_tag=tag_no,date=d1,amount=amount,status="not paid")
        acc.save()
        return HttpResponseRedirect('/canteenview/')

def payfine(request):
    today = date.today()
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    user=library.objects.filter(s_tag=tag_no,return_date=today).order_by('-id')[0]
    print(user)
    amount=user.fine
    st=student.objects.get(s_tag=tag_no)
    if st.balance >= amount:
        st.balance=int(st.balance)-int(amount)
        st.save()
        acc=account(s_tag=tag_no,date=today,amount=amount,status="paid")
        acc.save()
        mail=str(amount) +' TK fine is deducted from your account. Current balance is '+str(st.balance)+' TK.'
        mail_to=st.email
        send_mail('account', mail,'accnt12999@gmail.com',[mail_to])
        return HttpResponseRedirect('/libraryview/')
    else:
        acc=account(s_tag=tag_no,date=d1,amount=amount,status="not paid")
        acc.save()
        return HttpResponseRedirect('/libraryview/')
