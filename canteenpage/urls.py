from django.urls import path
from . import views

urlpatterns = [
    path('canteenview/',views.canteenview),
    path('bill/',views.bill),
]
