from django.apps import AppConfig


class CanteenpageConfig(AppConfig):
    name = 'canteenpage'
