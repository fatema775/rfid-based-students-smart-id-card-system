from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from datetime import date
import datetime
from homepage.models import canteen,student
import serial
from django.core.mail import send_mail

def canteenview(request):
    return render(request,'canteen.html')

def bill(request):
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    c1=request.POST['cost1']
    u1=request.POST['units1']
    c2=request.POST['cost2']
    u2=request.POST['units2']
    c3=request.POST['cost3']
    u3=request.POST['units3']
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    t_bill=int(c1)*int(u1)+int(c2)*int(u2)+int(c3)*int(u3)
    print(t_bill)
    if student.objects.filter(s_tag=tag_no).exists():
        user=student.objects.get(s_tag=tag_no)
        #messages.info(request,'Enter your pin: ')
        new_bill=canteen(s_tag=user.s_tag,date=d1,bill=t_bill)
        new_bill.save()
        x=datetime.datetime.now()
        if user.balance>=t_bill:
            nb=user.balance-int(t_bill)
            mail='Rice Item '+str(u1)+' Unit '+str(c1)+' Tk, Fast Food '+str(u2)+' Unit '+str(c2)+' Tk, Drinks '+str(u3)+' Unit '+str(c3)+' Tk. A total of '+str(t_bill)+' Tk is deducted from your account at '+str(x.strftime("%X"))+' '+str(date.today())+'. Current balance is '+str(nb)+' Tk.'
            mail_to=user.email
            send_mail('account', mail,'accnt12999@gmail.com',[mail_to])
        return HttpResponseRedirect('/payment/')
