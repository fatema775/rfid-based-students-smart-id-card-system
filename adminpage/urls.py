from django.urls import path
from . import views

urlpatterns = [
    path('adminview/',views.adminview),
    path('adduser/',views.adduser),
    path('addstudent/',views.addstudent),
]
