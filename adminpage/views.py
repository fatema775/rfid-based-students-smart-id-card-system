from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User,auth
from django.contrib import messages
from homepage.models import student
import serial

def adminview(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    else:
        return render(request,'admin.html')

def adduser(request):
    if request.method == 'POST':
        name=request.POST['name']
        uname=request.POST['uname']
        email=request.POST['email']
        role=request.POST['role']
        user=User.objects.create_superuser(first_name=name,username=uname,email=email,last_name=role)
        user.save()
        messages.info(request,'User Added Successfully')
        return HttpResponseRedirect('/adminview/')

def addstudent(request):
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    if student.objects.filter(s_tag=tag_no).exists():
        messages.info(request,'Student Exists')
    else:
        new_student=student(s_tag=tag_no,s_name=request.POST['sname'],s_roll=request.POST['roll'],pin=request.POST['pin'],email=request.POST['email'],dept=request.POST['dept'],session=request.POST['sess'],semester=request.POST['sem'],balance=0)
        new_student.save()
        messages.info(request,'Student Added Successfully')
    return HttpResponseRedirect('/adminview/')
