from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from homepage.models import student,library
from datetime import date
import serial
from django.core.mail import send_mail

def libraryview(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    else:
        return render(request,'library.html')

def issuebook(request):
    b=request.POST['bkid']
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    user=student.objects.get(s_tag=tag_no)
    request.session['tag']=tag_no
    request.session['bid']=b
    if user is not None:
        request.session['cnt']=0
        return render(request,'home3.html')
    else:
         messages.info(request,'not a user')
         return HttpResponseRedirect('/libraryview/')

def validissue(request):
    today = date.today()
    pin=request.POST['pin']
    tag=request.session['tag']
    b=request.session['bid']
    user=student.objects.get(s_tag=tag)
    if student.objects.filter(s_tag=tag,pin=pin).exists():
        new_book=library(s_tag=user.s_tag,book_id=b,issue_date=today,return_date=today,fine=0)
        new_book.save()
        messages.info(request,'Book Issued Successfully')
        return HttpResponseRedirect('/libraryview/')
    else:
        request.session['cnt']=request.session['cnt']+1
        if request.session['cnt'] == 3:
            print('cnt',request.session['cnt'])
            messages.info(request,'Try Again Letter')
            mail='Someone tried to access your card. Take necessary action.'
            mail_to=user.email
            send_mail('account', mail,'accnt12999@gmail.com',[mail_to])
            return HttpResponseRedirect('/libraryview/')
        print('count',request.session['cnt'])
        messages.info(request,'Wrong pin!!!!!!!')
        return render(request,'home3.html')

def returnbook(request):
    today = date.today()
    b=request.POST['bkid']
    arduino = serial.Serial('COM4', 9600, timeout=.1)
    while True:
        tag_no=arduino.readline().decode('ascii')
        if tag_no:
            break
    arduino.close()
    user=student.objects.get(s_tag=tag_no)
    print(user)
    if user is not None:
        upd=library.objects.get(s_tag=tag_no,book_id=b)
        upd.return_date=today
        upd.save()
        delta = today - upd.issue_date
        if (delta.days)>7:
            upd.fine=(delta.days)-int(7)
            upd.save()
            return HttpResponseRedirect('/payfine/')
        else:
            return HttpResponseRedirect('/libraryview/')
    else:
        messages_info(request,'not a user')
    return HttpResponseRedirect('/libraryview/')
