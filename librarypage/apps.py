from django.apps import AppConfig


class LibrarypageConfig(AppConfig):
    name = 'librarypage'
