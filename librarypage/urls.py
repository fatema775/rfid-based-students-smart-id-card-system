from django.urls import path
from . import views

urlpatterns = [
    path('libraryview/',views.libraryview),
    path('issuebook/',views.issuebook),
    path('validissue/',views.validissue),
    path('returnbook/',views.returnbook),
]
